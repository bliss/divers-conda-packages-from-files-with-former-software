#!/bin/bash
echo -e "\n\n\n\n\n\n\n\n\n"
# using the directory name to guess the name of the package file to be copied into the conda repo

set -x
#~ echo -e "\n\n\n\n\nConsider a conda build purge .\n\n\n\n\n"
conda build purge

echo -e "emptying source cache, which kept giving me the old script files."
conda clean -f --all --yes

conda build . --numpy 1.16 --channel=http://bcu-ci.esrf.fr/stable

#&& \
 #/bin/cp -p /opt/miniconda/conda-bld/linux-64/$(basename $(pwd))*.tar.bz2 /segfs/bliss/source/admin/conda/noarch

