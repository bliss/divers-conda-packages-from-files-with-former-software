#!/bin/bash -u

# post link (post installation) script for conda package tango_applis
# it aims to copy the launcher scripts for the various java applications
# to ~blissadm/bin, so to make it available without conda environment.
# A conda env should be created by the scripts.

# scripts are :
# jvacuum / jidappli
# jpss
# jvacuum / jvacuum2 / vacuum, jvacuum2_edit, jvacuum_appli
# astor, atkmoni, atkpanel, jdraw, jhdbconfig, jhdbviewer, jive, logviewer, synopticAppli

if [ -d "~blissadm/bin" ]; then
    for name in jvacuum jidappli jpss jvacuum jvacuum2 vacuum jvacuum2_edit jvacuum_appli astor atkmoni atkpanel jdraw jhdbconfig jhdbviewer jive logviewer synopticAppli
    do
        if [ -x "~blissadm/bin/$name" ]; then
            mv "~blissadm/bin/$name" "~blissadm/bin/.tango_appli_$name"
        fi
        ln -s ${PREFIX}/bin/${name} ~blissadm/bin/
    done
fi
