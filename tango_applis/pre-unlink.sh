#!/bin/bash -u

# pre unlink (pre UNinstallation) script for conda package tango_applis
# it aims to delete the launcher scripts for the various java applications
# from ~blissadm/bin and to put back files, that were possibly present
# at install time.

# scripts are :
# jvacuum / jidappli
# jpss
# jvacuum / jvacuum2 / vacuum, jvacuum2_edit, jvacuum_appli
# astor, atkmoni, atkpanel, jdraw, jhdbconfig, jhdbviewer, jive, logviewer, synopticAppli

if [ -d "~blissadm/bin" ]; then
    for name in jvacuum jidappli jpss jvacuum jvacuum2 vacuum jvacuum2_edit jvacuum_appli astor atkmoni atkpanel jdraw jhdbconfig jhdbviewer jive logviewer synopticAppli
    do
        rm -rf ~blissadm/bin/$name
    done

    # put back what was there at install time
    for name in "blabla ~blissadm/bin/.tango_appli_*"
    do
        if [ -x "$name" ]; then
            mv $name ${name/.tango_appli_}
        fi
    done
fi

