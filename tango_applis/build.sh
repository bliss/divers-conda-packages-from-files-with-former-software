#!/bin/bash -ue
# build.sh for tango-applis

# SRC_DIR is the place, where all the files are.
TANGO_JAVA_LIB=${PREFIX}/lib/java
#~ APPS_HOME=${PREFIX}/${PKG_NAME}

mkdir -p ${TANGO_JAVA_LIB}

# push the scripts into the bin directory
cp -p ${SRC_DIR}/scripts/* ${PREFIX}/bin

# push the jars over to a lib/java dir
cp -p ${SRC_DIR}/java/* ${TANGO_JAVA_LIB}

# push the jars over to a lib/java dir
cp -p ${SRC_DIR}/jrga/* ${TANGO_JAVA_LIB}

exit 0
