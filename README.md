# Divers conda packages from files with former software

Main goal of this repo is to host the files needed to build packages like 
vacuum gauge servers or the jvacuum application.

- [x]  PLCvacuumValve
- [x]  VacGaugeServer
- [x]  jvacuum2
- [x]  tango_java_lib
- [x]  atkpanel
- [x]  jpps
- [x]  Atkmoni
- [x]  jdraw
- [x]  jive
 
And possibly:

- [ ]  fsigmon [lt]
- [ ]  HdbConfigGui
- [ ]  jhdbviewer
- [ ]  hdbmon
- [ ]  staticmon

And from us:

- [x]  local_desktop_viewer



# Things to be observed:

Create a directory anywhere and place the a meta.yaml file in it. Copy one from 
this repo.

Building is done by:

```bash
conda build -c http://bcu-ci.esrf.fr/stable .
```

<!--!!! note
    I found that the option `--output-folder /segfs/bliss/source/admin/conda/`
    will put the files into place and update the indexes. However it messes up 
    the indexes, so it should not be used. -->

Despite the line in package: numpy, there are many error messages
about numpy falling back to version 1.11. Even a conda_build_config.yaml
file didn't help this error.

Following the build, it is needed that the package file be copied
to the destination in the BCU conda repository.
An example for a copy line could be :

```bash
cp -p /opt/conda/conda-bld/linux-64/YourNewlyBuiltPackage*.tar.bz2 /segfs/bliss/source/admin/conda/linux-64/
```

Following which, the **index of the repository** needs rebuilding; to be done on computer **lid00limace**, user **blissadm**, like so:

```bash
conda activate build
cd /segfs/bliss/source/admin/conda
conda index
```

Unfortunately, the gitlab build process seems to reset the permissions of
sub-directories, which makes that `conda index` mightn't work.
On my computer I installed a user CONDA (uid 9001), so that I can run conda-index
as that user and not be hindered by missing permissions.

Make sure, that the build number is handled appropriately. At several opportunities
I mixed up an installed package on the target system with a newly built package
file, when I didn't change the build number. Also, `conda install` on the target
system will grab the new package, when the build number has changed.

Ordinarily, Version numbers are 3 digits, seperated by dots.

WARNING: **one needs to mkdir the ${CONDA_PREFIX}/bin directory**, otherwise it will be
deleted and replaced with the shell script you're copying there! (or any other directory
you might be copying into!)

'NOTA BENE'
The local_desktop_viewer has been moved into the ansible installation, for 
workstations only, as we now rely on system packages installed with apt, which 
can't be obtained through the use of conda packages. 
